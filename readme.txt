Grid Insight AMR LOGGER Script

Copyright 2015 Grid Insight Corporation

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

---------------
 
This script connects to a serial device like the Grid Insight
OV-1 (a.k.a. "Oysterville) utility meter data receiver module
and logs the received data to disk.

See the source code for more information.

Send questions to info@gridinsight.com.

