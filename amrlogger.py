#!/usr/bin/python

########################################################################
#
#   Grid Insight AMR LOGGER Script
#   Last revised 19 May 2015
#
#   Copyright 2015 Grid Insight Corporation
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
########################################################################



# This script connects to a serial device like the Grid Insight
# OV-1 (a.k.a. "Oysterville) utility meter data receiver module
# and logs the received data to disk.
#
# Example usage:
#   python amrlogger.py --port=COM32 -a -r -l --dir="c:/Users/User/Desktop/log" --file="_amr_log_file.txt"

import serial
import re
import datetime
import string
import sys
import getopt
import time


# handle command line options
options, remainder = getopt.getopt(sys.argv[1:], "avlr", ['port=', 'file=', 'dir='])

amr_serial_port = ''
open_append = False
verbose = False
logging = False
gps_logging = False
roll_daily = False
f = None
logfilename = 'amrlog.txt'
dir = '.'

for opt, arg in options:
    if opt in ('--port'):
        amr_serial_port = arg
    elif opt in ('--file'):
        logfilename = arg
        logging = True
    elif opt in ('--dir'):
        dir = arg
    elif opt == '-l':
        logging = True
    elif opt == '-a':
        open_append = True
    elif opt == '-v':
        verbose = True
    elif opt == '-r':
        roll_daily = True

if verbose:
    print sys.argv
    print options
    print remainder
		
m = re.compile('.*(\$.*)')


def unix_time(dt):
    epoch = datetime.datetime.utcfromtimestamp(0)
    delta = dt - epoch
    return delta.total_seconds()

def unix_time_millis(dt):
    return unix_time(dt) * 1000.0

    
def daily_file_name(logfilename, now):
#    [base, suffix] = logfilename.split('.', 1)
    return "%s/%s%s" % (dir, now.strftime('%Y%m%d'), logfilename)

def is_open_file(f):
    return not ((f == None) or ((f is file) and f.closed()))
        
#ser_amr = serial.Serial('/dev/ttyUSB0', 115200, timeout=1)
if verbose:
    print "opening amr port at " + amr_serial_port
ser_amr = serial.Serial(amr_serial_port, 115200, timeout=1)

# Change the mode
try:
    ser_amr.flushInput()
    ser_amr.write('\n')
    time.sleep(1)
    ser_amr.flushInput()
    ser_amr.write('MODE 1\n') # go into SCM mode
    time.sleep(1)
    ser_amr.flushInput()
except ValueError:
    print "Serial port error"
    exit(1)

    
current_filename = dir + '/' + logfilename
last_flush = datetime.datetime.utcnow()
f = None
while True:
    # Match "$UMBOM"
    line = ser_amr.readline().rstrip()   # read a '\n' terminated line
    now = datetime.datetime.utcnow()
    if ((unix_time(now) - unix_time(last_flush)) > 10.0) and is_open_file(f):
        f.flush() # make sure we flush to disk at least every 10 seconds
        last_flush = now
    if (line != '') and verbose:
        print line
    g = m.search(line)
    if g:
        amr_text = string.replace(g.group(1),':',',')
        outstring = "%s,%sZ,\"%s\"\r\n" % (str(unix_time(now)), now.isoformat(), amr_text)
        sys.stdout.write(outstring)
        if logging:
            if roll_daily:
                file_name_now = daily_file_name(logfilename, now)
                if file_name_now != current_filename:
                    if is_open_file(f):
                        f.close()
                        f = None
                    current_filename = file_name_now # new name
            if not is_open_file(f):
                if verbose:
                    print "opening file " + current_filename
                if open_append:
                    if verbose:
                        print "for append"
                    f = open(current_filename, 'a')
                else:
                    f = open(current_filename, 'w')
            f.write(outstring)
#	else:
#		print "no match"


