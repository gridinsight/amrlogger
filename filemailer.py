#!/usr/bin/python

########################################################################
#
#   Grid Insight FILE MAILER Script
#   Last revised 19 May 2015
#
#   Copyright 2015 Grid Insight Corporation
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
########################################################################

# This little script zips and mails all .txt files in a specific folder
# Sent files get copied to a subdirectory called 'sent'
# The file attachments are one zip file per input file
# The mail account must me a Gmail or Google Apps account, but you can change
# the code if you need more flexibility
#
# Example usage:
#   python filemailer.py --dir=. --subject="Your text files" --smtp-pass=badpassword --to=foo@bar.com --from=baz@gmail.com -v

import sys
import getopt
import datetime
import os
import os.path
import re
import zipfile
import tempfile

# Import smtplib for the actual sending function
import smtplib

# Here are the email package modules we'll need
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.encoders import encode_base64

recipients = []
sender = ''
subject = 'message from filemailer.py'
body = 'Please see attached files sent at %s GMT' % (str(datetime.datetime.utcnow()))
zipfiles = []
smtp_pwd = ''
COMMASPACE = ', '
sent_dir = 'sent'

options, remainder = getopt.getopt(sys.argv[1:], "v", ['dir=','subject=','to=','from=','smtp-pass='])

verbose = False
path = '.'
file_suffix_regex = re.compile('.*\.txt$')


for opt, arg in options:
    if opt in ('--dir'):
        path = arg
    elif opt in ('--subject'):
        subject = arg
    elif opt in ('--to'):
        recipients = [arg,]
    elif opt in ('--from'):
        sender = arg
    elif opt in ('--smtp-pass'):
        smtp_pwd = arg
    elif opt == '-v':
        verbose = True

if verbose:
    print sys.argv
    print options
    print remainder

# create 'sent' directory if it doesn't exist
sent_path = os.path.join(path, sent_dir)
if not os.path.isdir(sent_path):
    os.mkdir(sent_path)

# inventory the files to be sent
filenames = os.listdir(path)
zipfiles = []
for filename in filenames:
    if file_suffix_regex.match(filename):
        zipfiles.append(filename)

# Create the container (outer) email message.
msg = MIMEMultipart()
msg['Subject'] = subject
# me == the sender's email address
# family = the list of all recipients' email addresses
msg['From'] = sender
msg['To'] = COMMASPACE.join(recipients)
msg.preamble = body

# attach files
for file_name in zipfiles:
    attachment = MIMEBase('application', 'zip')
    try:
        # if file doesn't open (e.g. if in use), it won't be attached
        fp = tempfile.TemporaryFile(prefix='amr', suffix='.zip')
        zip = zipfile.ZipFile(fp, 'w')
        zip.write(os.path.join(path,file_name))
        zip.close()
        fp.seek(0)
        attachment.set_payload(fp.read())
        encode_base64(attachment)
        attachment.add_header('Content-Disposition', 'attachment',
               filename=(file_name + '.zip'))
        msg.attach(attachment)
        fp.close()
        os.rename(os.path.join(path,file_name), os.path.join(sent_path,file_name))
        if verbose:
            print "File '%s' zipped and attached." % (file_name)
    except:
        fp.close()

# send the message
if verbose:
    print "Preparing to send from '%s' to '%s' using subject '%s'." % (sender, str(recipients), subject)

try:
    smtp_conn = smtplib.SMTP("smtp.gmail.com",587)
    smtp_conn.ehlo()
    smtp_conn.starttls()
    smtp_conn.ehlo
    smtp_conn.login(sender, smtp_pwd)
    smtp_conn.sendmail(sender, recipients, msg.as_string())
except:
    print "Unexpected error:", sys.exc_info()[0]
finally:
    smtp_conn.close()
if verbose:
    print "Sent."

